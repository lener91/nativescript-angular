import { Injectable } from '@angular/core';
import { getJSON, request } from "@nativescript/core/http";

import { DestinoSQLiteClient } from './destino-sqlite.model';
import { DestinoSQLCouchClient } from "./destino-couch.model";
import { DestinoViaje } from './destino-viaje.model';
import { Store } from '@ngrx/store';
import { AppState } from '../destino/destino.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './destino-viaje-state.model';

@Injectable()
export class DestinoApiClient{
    destinos: DestinoViaje[];
    api: string = 'https://39d608703e09.ngrok.io';

    constructor(private destinoSQLite: DestinoSQLiteClient, private destinoCouch: DestinoSQLCouchClient, private store: Store<AppState>){
        this.destinos = [];
     }

    add(d: DestinoViaje){
        this.destinos.push(d);
        this.store.dispatch(new NuevoDestinoAction(d));
    }

    agregar(d: DestinoViaje){
        return request({
            url: this.api,
            method: 'POST',
            headers: { "Content-Type": "application/json" },
            content: JSON.stringify({
                nuevo: d.nombre
            })
        });
    }

    getAll(): DestinoViaje[]{
        return this.destinos;
    }

    getAllJson(): DestinoViaje[]{
        getJSON(this.api + "/my").then((req: any) => {
            req.forEach((r) => {
                var dd = new DestinoViaje(r.nombre, r.u);
                
                this.destinos.push(dd);
            });
        }, (e) => console.log('error al obtener la informaciòn solicitada'));

        return this.destinos;
    }

    buscar(q: string){
        this.destinoSQLite.getDb((db) => {
            db.execSQL('insert into logs(texto) values(?)', [q], (err, id) => console.log('el nuevo logid', id));
        }, (error) => console.log('error sqlite: ', error));

        const documentId = this.destinoCouch.db.createDocument({ texto: q });
 console.log("nuevo id couchbase: ", documentId); 

        return getJSON(this.api + "/ciudades?q=" + q);
    }

    elegir(d: DestinoViaje){
        this.store.dispatch(new ElegidoFavoritoAction(d));
        this.destinoSQLite.getDb((db) => {
            db.execSQL('insert into favs(nombre, url) values(?, ?)', [d.nombre, d.u], (err, id) => console.log('el nuevo log id', id));
        }, (error) => console.log('error sqlite: ', error));
        //this.destinos.forEach(x => x.setSelected(false));
        d.setSelected(true);
    }

    noelegir(d: DestinoViaje){
        this.destinoSQLite.getDb((db) => {
            db.execSQL('delete into favs where nombre = ?', [d.nombre], (err, id) => console.log('el nuevo log id', id));
        }, (error) => console.log('error sqlite: ', error));

        d.setSelected(false);
    }

    getById(id: string): DestinoViaje{
        return this.destinos.filter((d) => { return d.id.toString() == id })[0];
    }

    getSearch(f: string): DestinoViaje[]{
        return this.destinos.filter((x) => x.nombre.indexOf(f) >= 0);
    }
}