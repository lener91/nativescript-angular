import { idProperty } from '@nativescript/core/ui/core/view-base';
import { ObjectUnsubscribedError } from 'rxjs';

export class DestinoViaje{
    public selected: boolean;
    public servicios: string[];
    id = +1;

    constructor(public nombre: string, public u: string = 'res://img1', public votes: number = 0){
        this.servicios = ['piscina', 'desayuno'];
    }

    isSelected(): boolean{
        return this.selected;
    }

    setSelected(s: boolean): void{
        this.selected = s;
    }

    voteUp(){
        this.votes++;
    }
    
    voteDown(){
        this.votes--;
    }
}