import { Injectable } from '@angular/core';
const sqlite = require('nativescript-sqlite');

@Injectable()
export class DestinoSQLiteClient{
    constructor(){
        this.getDb((db) => {
            db.each('select * from logs', (err, fila) => console.log('fila', fila),
            (err, totales) => console.log('totales: ', totales))
        }, (error) => console.log('error en geDb', error));
    }

    getDb(fnOk, fnError){
        return new sqlite('my_db_logs', (err, db) => {
            if(err){ console.log(err); }
            else{
                db.execSQL('CREATE TABLE IF NOT EXISTS logs (id INTEGER PRIMARY KEY AUTOINCREMENT, texto TEXT)').then(id => {
                    fnOk(db);
                }, (error) => {
                    console.log('error create: ', error);
                    fnError(error);
                });

                db.execSQL('CREATE TABLE IF NOT EXISTS favs (id INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT, url TEXT)').then(id => {
                    fnOk(db);
                }, (error) => {
                    console.log('error create: ', error);
                    fnError(error);
                });
                //
            }
        });
    }
}
