import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map } from 'rxjs/operators';

import { DestinoViaje } from './destino-viaje.model';

// Estado
export interface DestinoViajeState {
    items: DestinoViaje[];
    loading: boolean;
    favorito: DestinoViaje;
}

export function initializeDestioViajeState(){
    return {
        items: [],
        loading: false,
        favorito: null
    }
}

// Acciones
export enum DestinoViajeActionTypes {
    NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
    ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito',
    VOTE_UP = '[Destinos Viajes] Vote Up',
    VOTE_DOWN = '[Destinos Viajes] Vote Down',
    INIT_MY_DATA = '[Destinos Viajes] Init My Data'
}

export class NuevoDestinoAction implements Action {
    type = DestinoViajeActionTypes.NUEVO_DESTINO;
    constructor(public destino: DestinoViaje){}
}

export class ElegidoFavoritoAction implements Action {
    type = DestinoViajeActionTypes.ELEGIDO_FAVORITO;
    constructor(public destino: DestinoViaje){}
}

export class VoteUpAction implements Action {
    type = DestinoViajeActionTypes.VOTE_UP;
    constructor(public destino: DestinoViaje){}
}

export class VoteDownAction implements Action {
    type = DestinoViajeActionTypes.VOTE_DOWN;
    constructor(public destino: DestinoViaje){}
}

export class InitMyDataAction implements Action {
    type = DestinoViajeActionTypes.INIT_MY_DATA;
    constructor(public destinos: string[]){}
}

export type DestinoViajeActions = NuevoDestinoAction | ElegidoFavoritoAction | VoteUpAction | VoteDownAction | InitMyDataAction;

// reductores
export function reducerDestinosViajes (
    state: DestinoViajeState,
    action: DestinoViajeActions
): DestinoViajeState{
    switch (action.type){
        case DestinoViajeActionTypes.INIT_MY_DATA:{
            const destinos: string[] = (action as InitMyDataAction).destinos;
            return {
                ...state,
                items: destinos.map((d) => new DestinoViaje(d, ''))
            };
        }
        case DestinoViajeActionTypes.NUEVO_DESTINO:{
            return {
                ...state,
                items: [...state.items, (action as ElegidoFavoritoAction).destino]
            };
        }
        case DestinoViajeActionTypes.ELEGIDO_FAVORITO:{
            state.items.forEach((x) => x.setSelected(false));
            let fav: DestinoViaje = (action as ElegidoFavoritoAction).destino;
            fav.setSelected(true);
            return {
                ...state, 
                favorito: fav
            };
        }
        case DestinoViajeActionTypes.VOTE_UP:{
            const d: DestinoViaje = (action as VoteUpAction).destino;
            d.voteUp();
            return { ...state };
        }
        case DestinoViajeActionTypes.VOTE_DOWN:{
            const d: DestinoViaje = (action as VoteDownAction).destino;
            d.voteDown();
            return { ...state };
        }
    }
    return state;
}

// efectos
@Injectable()
export class DestinoViajeEffects {
    @Effect()
    nuevoAgregado$: Observable<Action> = this.actions$.pipe(
        ofType(DestinoViajeActionTypes.NUEVO_DESTINO),
        map((action: NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
    );

    constructor(private actions$: Actions){}
}