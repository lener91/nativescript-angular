import { Injectable } from '@angular/core';
import * as couchbase from 'nativescript-couchbase'; 

@Injectable()
export class DestinoSQLCouchClient{
    public db: couchbase.Couchbase;

    constructor(){
        this.db = new couchbase.Couchbase('couch_database');

        this.db.createView("logs", "1", (document, emitter) => emitter.emit(document._id, document));
        const rows = this.db.executeQuery("logs", {limit : 200});
        console.log("documentos: " + JSON.stringify(rows)); 
    }
}