import { initializeDestioViajeState, InitMyDataAction, reducerDestinosViajes, NuevoDestinoAction } from "./destino-viaje-state.model";
import { DestinoViaje } from "./destino-viaje.model";

describe("reducersNoticias", function () {
    it("should reduce init data", function () {
        // setup
        var prevState = initializeDestioViajeState();
        var action = new InitMyDataAction(["noticia 1", "noticia 2"]);
        // action
        var newState = reducerDestinosViajes(prevState, action);
        // assertions
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].nombre).toEqual("noticia 1");
    });
    
    it("should reduce new item added", function () {
        var prevState = initializeDestioViajeState();
        var action = new NuevoDestinoAction(new DestinoViaje("noticia 3"));
        var newState = reducerDestinosViajes(prevState, action);
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].nombre).toEqual("noticia 3");
    });
});