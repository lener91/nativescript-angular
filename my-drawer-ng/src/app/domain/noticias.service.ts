import { Injectable } from "@angular/core";

@Injectable()
export class NoticiasService
{
    private noticias: Array<string> = [];

    agregar(s: string): void{
        this.noticias.push(s);
    }

    buscar(): Array<string>{
        return this.noticias;
    }
}