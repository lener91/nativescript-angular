import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Application } from "@nativescript/core";
import * as Toast from "nativescript-toasts";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";

import { DestinoViaje } from "../models/destino-viaje.model";
import { DestinoApiClient } from "../models/destino-api-client.model";
import { RouterExtensions } from "@nativescript/angular";

@Component({
    selector: "app-detalle-destino",
    templateUrl: "./detalle.component.html",
})
export class DestinoDetalleComponent implements OnInit {
    destino: DestinoViaje;

    constructor(
        private route: ActivatedRoute,
        private apiClient: DestinoApiClient,
        private routerExtensions: RouterExtensions
    ) {}

    ngOnInit(): void {
        const id = this.route.snapshot.paramMap.get("id");
        this.apiClient.getAll().forEach((item, idx) => {
            if (idx.toString() === id) {
                this.destino = item;
            }
        });

        let toastOptions:Toast.ToastOptions = {text: "Detalle " + this.destino.nombre, duration: Toast.DURATION.LONG};
        Toast.show(toastOptions);
    }
    
    goBack() {
        this.routerExtensions.backToPreviousPage();
    }

    urlstrin(u: string): string {
        return "res://" + u;
    }
}
