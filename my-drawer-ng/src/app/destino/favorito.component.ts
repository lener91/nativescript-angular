import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import * as Toast from "nativescript-toasts";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";

import { DestinoViaje } from "../models/destino-viaje.model";
import { DestinoApiClient } from "../models/destino-api-client.model";
import { RouterExtensions } from "@nativescript/angular";
import { DestinoSQLiteClient } from "../models/destino-sqlite.model";

@Component({
    selector: "app-favorito-destino",
    templateUrl: "./favorito.component.html",
})
export class DestinoFavoritoComponent implements OnInit {
    destinos: DestinoViaje[];

    constructor(private apiSQLite: DestinoSQLiteClient
    ) {
        this.destinos = [];
    }

    ngOnInit(): void {
        this.apiSQLite.getDb((db) => {
            db.each('select * from favs', (err, fila) => {
                let dd = new DestinoViaje(fila[1], fila[2]);

                this.destinos.push(dd);
            },
            (err, totales) => console.log('totales: ', totales))
        }, (error) => console.log('error en geDb', error));
    }
    
    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    urlstrin(u: string): string {
        return u;
    }
}
