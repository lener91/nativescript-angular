import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";
import { alert } from "@nativescript/core/ui/dialogs";
import { ActivatedRoute } from "@angular/router";
import { RouterExtensions } from "@nativescript/angular";

import { DestinoViaje } from "../models/destino-viaje.model";
import { DestinoApiClient } from "../models/destino-api-client.model";

@Component({
    selector: "FormDestino",
    templateUrl: "./form.component.html"
})
export class FormDestinoComponent implements OnInit {
    dest: DestinoViaje;
    id: string;

    constructor(private destionoApiClient: DestinoApiClient, private route: ActivatedRoute, private routerExtensions: RouterExtensions) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        this.id = this.route.snapshot.paramMap.get("id");
        
        if(this.id === null){ this.dest = new DestinoViaje('', ''); }
        else{
            this.destionoApiClient.getAll().forEach((item, idx) => {
                if (idx.toString() === this.id) { this.dest = item; }
            });
        }
    }

    goBack() {
        this.routerExtensions.backToPreviousPage();
    }

    onAddTap(): void{
        if(this.id === null)
        { 
            this.destionoApiClient.add(this.dest);
            
            alert({
                title: 'Correcto',
                message: 'El destino fue ingresado con exito',
                okButtonText: 'Aceptar'
            }).then((result) => {
                this.routerExtensions.navigate(['listadodestino'], {
                    transition: {
                        name: "fade",
                    },
                });
            });
        }
        else{
            this.destionoApiClient.getAll().forEach((item, i) =>{
                if(i.toString() === this.id){ 
                    item.nombre = this.dest.nombre
                    item.u = this.dest.u
                }
            });

            alert({
                title: 'Correcto',
                message: 'El destino fue actualizado con exito',
                okButtonText: 'Aceptar'
            }).then((result) => {
                this.routerExtensions.navigate(['listadodestino'], {
                    transition: {
                        name: "fade",
                    },
                });
            });
        }    
    }
}