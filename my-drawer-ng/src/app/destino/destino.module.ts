import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule, NativeScriptFormsModule } from "@nativescript/angular";
import { ActionReducerMap, StoreModule as NgRxStoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { DestinoApiClient } from "../models/destino-api-client.model";
import { DestinoSQLiteClient } from "../models/destino-sqlite.model";
import { DestinoSQLCouchClient } from "../models/destino-couch.model";
import { DestinoRoutingModule } from "./destino-routing.module";
import { ListadoDestinoComponent } from "./listado.component";
import { DestinoDetalleComponent } from "./detalle.component";
import { SearchDestinoComponent } from './search.component';
import { FormDestinoComponent } from "./form.component";
import { DestinoFavoritoComponent } from "./favorito.component";
import { DestinoViajeEffects, DestinoViajeState, initializeDestioViajeState, reducerDestinosViajes } from "../models/destino-viaje-state.model";
import { MailHelpers } from "../helpers/mail.helper";

export interface AppState{
    destinos: DestinoViajeState;
}

const reducers: ActionReducerMap<AppState> = {
    destinos: reducerDestinosViajes
}

let reducersInitialState = {
    destinos: initializeDestioViajeState()
}

@NgModule({
    imports: [
        NativeScriptCommonModule,
        DestinoRoutingModule,
        NativeScriptFormsModule,
        NgRxStoreModule.forRoot(reducers, { initialState: reducersInitialState, 
            runtimeChecks: {
              strictStateImmutability: false,
              strictActionImmutability: false,
            } 
        }),
        EffectsModule.forRoot([DestinoViajeEffects])
    ],
    declarations: [
        ListadoDestinoComponent,
        DestinoDetalleComponent,
        SearchDestinoComponent,
        FormDestinoComponent,
        DestinoFavoritoComponent
    ],
    providers: [DestinoApiClient, DestinoSQLiteClient, DestinoSQLCouchClient, MailHelpers],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class ListadoDestinoModule { }
