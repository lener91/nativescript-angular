import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'SearchDestino',
  templateUrl: './search.component.html',
  moduleId: module.id
})
export class SearchDestinoComponent implements OnInit {
  textFieldValue: string = "";
  @Output() search: EventEmitter<string> = new EventEmitter();

  ngOnInit(): void { 
    this.textFieldValue = "managua";
  }

  onButtonTap(): void{
    if(this.textFieldValue.length > 2)
    { this.search.emit(this.textFieldValue); }
  }

}
