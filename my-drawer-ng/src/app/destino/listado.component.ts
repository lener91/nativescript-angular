import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application, Color, GestureEventData, View, ImageSource, fromObject } from "@nativescript/core";
import { SetupItemViewArgs } from "@nativescript/angular/directives/templated-items-comp";
import { action, alert } from "@nativescript/core/ui/dialogs";
//import {AnimationCurve} from "tns-core-modules/ui/enums";
import { RouterExtensions } from "@nativescript/angular";
import { registerElement } from '@nativescript/angular';
import { shareImage } from "nativescript-social-share";
import * as Toast from "nativescript-toasts";
registerElement('Fab', () => require('@nstudio/nativescript-floatingactionbutton').Fab);

import { DestinoViaje } from "../models/destino-viaje.model";
import { DestinoApiClient } from "../models/destino-api-client.model";
import { Store } from "@ngrx/store";
import { AppState } from "./destino.module";
import { MailHelpers } from "../helpers/mail.helper";
//import { ElegidoFavoritoAction, NuevoDestinoAction } from '../../models/destino-viaje-state.model';

@Component({
    selector: "app-listado-destino",
    templateUrl: "./listado.component.html",
})
export class ListadoDestinoComponent implements OnInit {
    resultado: DestinoViaje[];
    favorito: DestinoViaje;
    @ViewChild("layout") layout: ElementRef;
    @ViewChild("view") view: ElementRef;
    
    constructor(
        public destinoApiClient: DestinoApiClient,
        private routerExtensions: RouterExtensions,
        private store: Store<AppState>, 
        private mail: MailHelpers
    ) {
        this.resultado = [];
        this.favorito = new DestinoViaje('', '');
        this.store.select((state) => state.destinos.favorito).subscribe((data) => {
            if(data !== null)
            {
                this.favorito = data;
                Toast.show({text: 'favorito: ' + data.nombre, duration: Toast.DURATION.LONG});
            }
        });
    }

    ngOnInit(): void {
       this.resultado = this.destinoApiClient.getAllJson();
    }

    searchDestino(f: string){
        this.destinoApiClient.buscar(f).then((r: any) => {
            r.forEach(r => {
                var dd = new DestinoViaje(r.nombre, r.u);
                this.resultado.push(dd);
            });
        }, (e) => {
            Toast.show({text: 'Error en la busqueda!', duration: Toast.DURATION.LONG});
        });

        const layo = <View>this.layout.nativeElement;
        layo.animate({
            backgroundColor: new Color('blue'),
            duration: 300,
            delay: 150
        }).then(() => layo.animate({
            backgroundColor: new Color('white'),
            duration: 300,
            delay: 150
        }));

        // const view = <View>this.view.nativeElement;
        // view.animate({
        //     translate: { x: 0, y: 100},
        //     duration: 1000//,
        //     //curve: AnimationCurve.easeIn
        // });
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onPull(e) {
        console.log(e);
        const pullRefresh = e.object;
        const d = new DestinoViaje('probando', 'res://img3');

        setTimeout(() => {
            this.resultado.push(d);
            pullRefresh.refreshing = false;
        }, 2000);
    }

    onItemTap(x: SetupItemViewArgs): void {
        let dest: DestinoViaje;
        let idx = x.index.toString();
        const url = "listadodestino/editar/" + idx;

        this.destinoApiClient.getAll().forEach((item, i) => {
            if (idx === i.toString()) { dest = item; }
        });

        action("Mensaje", "Editar!", dest.servicios).then((result) => {
            if(result === 'Editar!')
            {
                this.routerExtensions.navigate([url], {
                    transition: {
                        name: "fade",
                    },
                });
            }
        });
    }

    onTapFav(e){
        let d: DestinoViaje = this.destinoApiClient.getSearch(e)[0];
        console.log(d)
        this.destinoApiClient.elegir(d);
    }

    onTapNoSocial(e){
       // shareText(e, 'Asunto: compartido desde el curso');
       let imgFromResources: ImageSource;
       ImageSource.fromResource("icon").then((img) => {
           imgFromResources = img;
       });
       shareImage(imgFromResources, 'Asunto: probando la app');
    }

    onTapNoFav(e){
        let d: DestinoViaje = this.destinoApiClient.getSearch(e)[0];

        this.destinoApiClient.noelegir(d);
    }

    onTapNoSendMail(){
        const attach =  [{ fileName: "arrow1.png", path:"base64://iVBORw0KGgoAAAANSUhEUgAAABYAAAAoCAYAAAD6xArmAAAACXBIWXMAABYlAAAWJQFJUiTwAAAAHGlET1QAAAACAAAAAAAAABQAAAAoAAAAFAAAABQAAAB5EsHiAAAAAEVJREFUSA1iYKAimDhxYjwIU9FIBgaQgZMmTfoPwlOmTJGniuHIhlLNxaOGwiNqNEypkwlGk9RokoIUfaM5ijo5Clh9AAAAAP//ksWFvgAAAEFJREFUY5g4cWL8pEmToMwiM1ATTBqONbQHA2W0WDBGgJYBUdTy2iwYA0BrILDI7VMmTJFHqv3yBUEBQsIg/QDAJNpcv6v+k1ZAAAAAElFTkSuQmCC", mimeType: "image/png" }];

        this.mail.sendMail('Envio de nativescrit de prueba', 'Hola <strong>mundo!</strong> :)"', ['helpinfo@gmail.com'], [], [], attach);
    }

    urlstrin(u: string): string {
        return u;
    }

    onLongPress(e: GestureEventData){
        console.log(e.view);
        console.log(e.eventName);
    }
    
    fabTap(){
        this.routerExtensions.navigate(["listadodestino/agregar"], {
            transition: {
                name: "fade",
            },
        });
    }
}
