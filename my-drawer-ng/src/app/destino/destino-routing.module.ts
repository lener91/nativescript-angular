import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "@nativescript/angular";

import { ListadoDestinoComponent } from "./listado.component";
import { DestinoDetalleComponent } from "./detalle.component";
import { FormDestinoComponent } from "./form.component";
import { DestinoFavoritoComponent } from "./favorito.component";

const routes: Routes = [
    { path: "", component: ListadoDestinoComponent },
    { path: "detalle/:id", component: DestinoDetalleComponent },
    { path: "agregar", component: FormDestinoComponent },
    { path: "editar/:id", component: FormDestinoComponent },
    { path: "favorito", component: DestinoFavoritoComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class DestinoRoutingModule { }