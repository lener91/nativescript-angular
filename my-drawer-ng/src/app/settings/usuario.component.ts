import { Component, OnInit } from "@angular/core";
import * as appSetting from "@nativescript/core/application-settings";
import { RouterExtensions } from "@nativescript/angular";

@Component({
    selector: "Usuario",
    templateUrl: "./usuario.component.html"
})
export class UsuarioComponent implements OnInit {
    nombreUsuario: string = '';

    constructor(private routerExtensions: RouterExtensions){
        
    }

    ngOnInit(): void{
        if(appSetting.getString('nombreUsuario') !== undefined)
        { this.nombreUsuario = appSetting.getString('nombreUsuario'); }
    }

    onButtonTap(){
        if(this.nombreUsuario.length > 2)
        { 
            appSetting.setString('nombreUsuario', this.nombreUsuario);

            this.routerExtensions.navigate(['settings'], {
                transition: {
                    name: "fade",
                },
            });
        }
    }

    goBack() {
        this.routerExtensions.backToPreviousPage();
    }
}
