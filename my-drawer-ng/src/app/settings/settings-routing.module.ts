import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "@nativescript/angular";

import { SettingsComponent } from "./settings.component";
import { UsuarioComponent } from "./usuario.component";

const routes: Routes = [
    { path: "", component: SettingsComponent },
    { path: "configuraciones", component: UsuarioComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class SettingsRoutingModule { }
