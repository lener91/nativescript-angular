import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";
import * as appSetting from "@nativescript/core/application-settings";
import * as dialogs from "@nativescript/core/ui/dialogs";
import * as Toast from "nativescript-toasts";
import { RouterExtensions } from "@nativescript/angular";

@Component({
    selector: "Settings",
    templateUrl: "./settings.component.html"
})
export class SettingsComponent implements OnInit {
    nombreUsuario: string = '';

    constructor(private routerExtensions: RouterExtensions) {
        // Use the component constructor to inject providers.
    }

    doLoader(fn){ setTimeout(fn, 1000); }

    ngOnInit(): void {
        // Init your component properties here.
        // this.doLoader(() => dialogs.action('Mensaje', 'Cancelar!', ['Opciòn1', 'Opciòn2']).then((result) => {
        //     console.log('resultado' + result);
        //     if(result === 'Opciòn1'){
        //         this.doLoader(() => dialogs.alert({
        //             title: 'Titulo 1',
        //             message: 'msg 1',
        //             okButtonText: 'btn1'
        //         }).then(() => console.log('cerrado 1!')));
        //     }else if(result === 'Opciòn2'){
        //         this.doLoader(() => dialogs.alert({
        //             title: 'Titulo 2',
        //             message: 'msg 2',
        //             okButtonText: 'btn2'
        //         }).then(() => console.log('cerrado 2!')));
        //     }
        // }));
        let toastOptions:Toast.ToastOptions = {text: "Hello World", duration: Toast.DURATION.SHORT};
        Toast.show(toastOptions);

        this.nombreUsuario = appSetting.getString('nombreUsuario');
    }

    onButtonTap(){
        this.routerExtensions.navigate(['settings/configuraciones'], {
            transition: {
                name: "fade",
            },
        });
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }
}
