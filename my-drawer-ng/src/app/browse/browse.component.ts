import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";
import { registerElement } from '@nativescript/angular';
import * as gmaps from "nativescript-google-maps-sdk";
registerElement('MapView', () => require('nativescript-google-maps-sdk').MapView);

@Component({
    selector: "Browse",
    templateUrl: "./browse.component.html"
})
export class BrowseComponent implements OnInit {
    @ViewChild("MapView") mapView: ElementRef;

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onMapReady(args): void { 
        const mapView = args.object;
        const marker = new gmaps.Marker();

        marker.position = gmaps.Position.positionFromLatLng(-34.6037, -58.3817);
        marker.title = 'Buenos aires';
        marker.snippet = 'Argentina';
        marker.userData = { index: 1 };

        mapView.addMarker(marker);
    }
}
