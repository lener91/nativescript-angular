import { compose } from "nativescript-email";
import { knownFolders } from "@nativescript/core/file-system";
import { Injectable } from "@angular/core";

@Injectable()
export class MailHelpers {
    appPath: string;
    logoPath: string;
    appFolder = knownFolders.currentApp();
    
    constructor(){
        this.appPath = this.appFolder.path;
        this.logoPath = this.appPath + "/res/desayuno.png"; 
    }

    sendMail(subj: string, body: string, to: string[], cc: string[], bcc: string[], attach: { fileName: string, path: string, mimeType: string }[]){
        compose({ subject: subj, body: body, to: to, cc: cc, bcc: bcc, attachments: attach })
            .then(() => console.log("Enviador de mail cerrado"), (err) => console.log("Error: " + err)); 
    }
}
