import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";
import * as camera from "@nativescript/camera";
import { ImageSource } from "@nativescript/core/image-source";
import { shareImage } from "nativescript-social-share";
import { Device, Screen } from "@nativescript/core/platform";
import * as Toast from "nativescript-toasts";

import {
    connectionType,
    getConnectionType,
    startMonitoring,
    stopMonitoring,
} from "@nativescript/core/connectivity";

@Component({
    selector: "Home",
    templateUrl: "./home.component.html",
})
export class HomeComponent implements OnInit {
    monitoreando: boolean = false;

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
        Toast.show({
            text: "código único de dispositivo" + Device.uuid,
            duration: Toast.DURATION.LONG,
        });
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onButtonTappInfo(): void {
        console.log("modelo", Device.model);
        console.log("tipo dispositivo", Device.deviceType);
        console.log("Sistema operativo", Device.os);
        console.log("versión sist operativo", Device.osVersion);
        console.log("Versión sdk", Device.sdkVersion);
        console.log("lenguaje", Device.language);
        console.log("fabricante", Device.manufacturer);
        console.log("código único de dispositivo", Device.uuid);
        console.log(
            "altura en pixels normalizados",
            Screen.mainScreen.heightDIPs
        );
        console.log("altura pixels", Screen.mainScreen.heightPixels);
        console.log("escala pantalla", Screen.mainScreen.scale);
        console.log("ancho pixels normalizados", Screen.mainScreen.widthDIPs);
        console.log("ancho pixels", Screen.mainScreen.widthPixels);
    }

    onButtonTappNetwork(): void {
        const myConnectionType = getConnectionType();

        switch (myConnectionType) {
            case connectionType.none:
                console.log("Sin Conexion");
                break;
            case connectionType.wifi:
                console.log("WiFi");
                break;
            case connectionType.mobile:
                console.log("Mobile");
                break;
            case connectionType.ethernet:
                console.log("Ethernet"); // es decir, cableada
                break;
            case connectionType.bluetooth:
                console.log("Bluetooth");
                break;
            default:
                break;
        }
        
        this.monitoreando = !this.monitoreando;
        if (this.monitoreando) {
            startMonitoring((newConnectionType) => {
                switch (newConnectionType) {
                    case connectionType.none:
                        console.log("Cambió a sin conexión.");
                        break;
                    case connectionType.wifi:
                        console.log("Cambió a WiFi.");
                        break;
                    case connectionType.mobile:
                        console.log("Cambió a mobile.");
                        break;
                    case connectionType.ethernet:
                        console.log("Cambió a ethernet.");
                        break;
                    case connectionType.bluetooth:
                        console.log("Cambió a bluetooth.");
                        break;
                    default:
                        break;
                }
            });
        } else {
            stopMonitoring();
        }
    }

    onButtonTap(): void {
        camera.requestCameraPermissions().then(
            function success() {
                const options = {
                    width: 300,
                    height: 300,
                    keepAspectRatio: false,
                    saveToGallery: true,
                };

                camera
                    .takePicture(options)
                    .then((imgAssets) => {
                        ImageSource.fromAsset(imgAssets)
                            .then((imgSource) => {
                                shareImage(imgSource, "Asunto: test");
                            })
                            .catch((err) => console.log("error source: ", err));
                    })
                    .catch((err) => console.log(err));
            },
            function failure() {
                console.log("permiso dnegado");
            }
        );
    }
}
